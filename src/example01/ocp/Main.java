package example01.ocp;

public class Main {

  public static void main(String[] args) {

    var emissaoExtratoRefatorado = new EmissaoExtrato();
    var extrato = emissaoExtratoRefatorado.emitir(TipoExtrato.CONTA_CORRENTE);
    System.out.println(extrato);
  }


}
